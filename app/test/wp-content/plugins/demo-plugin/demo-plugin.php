<?php
/*
Plugin Name: Demo Plugin
Description: Probegin demo plugin
Version: 1.0
Text Domain: demo-plugin
*/

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

require_once plugin_dir_path(__FILE__) . 'Demo_Controller.php';

class Demo_Plugin {

    /**
     * Constructor for Demo_Plugin class.
     */
    public function __construct() {
        add_action( 'admin_init', array( $this, 'check_dependencies' ) );
    }

    /**
     * Check if WooCommerce is installed and active.
     */
    public function check_dependencies() {
        if ( ! class_exists( 'WooCommerce' ) || ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
            // WooCommerce is not active, prevent activation of the plugin.
            deactivate_plugins( plugin_basename( __FILE__ ) );
            add_action( 'admin_notices', array( $this, 'woocommerce_dependency_notice' ) );
        }
    }

    /**
     * Display an admin notice for WooCommerce dependency.
     */
    public function woocommerce_dependency_notice() {
        echo '<div class="error"><p>';
        _e( 'Demo Plugin requires WooCommerce to be installed and activated. The plugin has been deactivated.', 'demo-plugin' );
        echo '</p></div>';
    }
}

new Demo_Plugin();
new Demo_Controller();