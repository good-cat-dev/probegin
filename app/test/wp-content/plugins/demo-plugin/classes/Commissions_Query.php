<?php


class Commissions_Query
{
    /**
     * Get available commissions data
     */
    public function get_available_commissions()
    {
        $commissions = [];

        // Create a query to fetch commissions from the custom post type "Commissions"
        $args = [
            'post_type' => 'commissions', // Replace with your post type if different
            'posts_per_page' => -1, // Get all commissions
        ];

        $query = new WP_Query($args);

        // Iterate through query results and add them to the $commissions array
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $commission_id = get_the_ID();
                $commission_title = get_the_title();

                // Get the minimum and maximum price values
                $min_price = get_post_meta($commission_id, '_product_min_price', true);
                $max_price = get_post_meta($commission_id, '_product_max_price', true);

                // Add the commission data to the array
                $commissions[$commission_id] = array(
                    'title' => $commission_title,
                    'min_price' => $min_price,
                    'max_price' => $max_price,
                );
            }
        }

        wp_reset_postdata();

        return $commissions;
    }
}