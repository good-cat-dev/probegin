<?php


class Commissions_Post_Type
{
    /**
     * Commissions_Post_Type constructor.
     */
    public function __construct()
    {
        // Hooks and actions
        add_action('init', array($this, 'register_commissions_post_type'));
        add_action('add_meta_boxes', array($this, 'add_commission_metabox'));
        add_action('save_post_commissions', array($this, 'save_commission_settings'));
    }

    /**
     * Register custom post type "Commissions"
     */
    public function register_commissions_post_type()
    {
        $labels = array(
            'name' => 'Commissions',
            'singular_name' => 'Commission',
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'menu_icon'   => 'dashicons-money-alt',
            'show_ui' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array( 'title' ),
            'register_meta_box_cb' => array($this, 'add_commission_metabox'), // Updated callback function
        );

        register_post_type('commissions', $args);
    }

    /**
     * Add metabox for commission settings
     */
    public function add_commission_metabox()
    {
        add_meta_box(
            'commission_settings',
            'Commission Settings',
            array($this, 'render_commission_metabox'), // Updated callback function
            'commissions',
            'normal',
            'high'
        );
    }

    /**
     * Render the HTML for the commission settings metabox
     */
    function render_commission_metabox($post) {
        // Retrieve the commission settings
        $commission_value = get_post_meta($post->ID, '_commission_value', true);
        $product_min_price = get_post_meta($post->ID, '_product_min_price', true);
        $product_max_price = get_post_meta($post->ID, '_product_max_price', true);

        // Output the HTML for the metabox
        echo '<label for="commission_value">Commission Value:</label>';
        echo '<input type="text" id="commission_value" name="commission_value" value="' . esc_attr($commission_value) . '" /><br /><hr />';

        echo '<label for="product_min_price">Product Min. Price:</label>';
        echo '<input type="number" id="product_min_price" name="product_min_price" value="' . esc_attr($product_min_price) . '" /><br /><hr />';

        echo '<label for="product_max_price">Product Max. Price:</label>';
        echo '<input type="number" id="product_max_price" name="product_max_price" value="' . esc_attr($product_max_price) . '" /><br /><hr />';
    }

    /**
     * Save commission settings when editing commissions post type
     */
    function save_commission_settings($post_id) {
        if (isset($_POST['commission_value'])) {
            update_post_meta($post_id, '_commission_value', sanitize_text_field($_POST['commission_value']));
        }

        if (isset($_POST['product_min_price'])) {
            update_post_meta($post_id, '_product_min_price', sanitize_text_field($_POST['product_min_price']));
        }

        if (isset($_POST['product_max_price'])) {
            update_post_meta($post_id, '_product_max_price', sanitize_text_field($_POST['product_max_price']));
        }
    }
}