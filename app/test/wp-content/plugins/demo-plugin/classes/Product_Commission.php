<?php
require_once 'Commissions_Query.php';

class Product_Commission
{
    /**
     * Product_Commission constructor.
     */
    public function __construct()
    {
        // Hooks and actions
        add_filter('woocommerce_product_data_tabs', array($this, 'add_commissions_tab'));
        add_filter('woocommerce_product_data_panels', array($this, 'commissions_tab_content'));
        add_action('woocommerce_process_product_meta', array($this, 'save_product_commission'), 999, 2);
        add_filter('woocommerce_product_get_price', array($this, 'apply_commission_to_product_price'), 10, 2);
        add_filter('woocommerce_product_variation_get_price', array($this, 'apply_commission_to_product_price'), 10, 2);
    }

    /**
     * Add a commissions tab to the product data
     */
    public function add_commissions_tab($tabs)
    {
        $tabs['commission_options'] = [
            'label'    => __('Commissions', 'your-text-domain'),
            'target'   => 'commission_options',
            'class'    => array('show_if_simple', 'show_if_variable'),
        ];
        return $tabs;
    }

    /**
     * Render content for the commissions tab
     */
    function commissions_tab_content() {
        global $post;
        $commissions_query = new Commissions_Query();

        // Load existing commission for this product, if any
        $product_commission = get_post_meta($post->ID, '_product_commission', true);

        // Get a list of available commissions
        $commissions = $commissions_query->get_available_commissions();

        // Dropdown for selecting a commission
        echo '<div id="commission_options" class="panel woocommerce_options_panel">';
        echo '<p class="form-field">';
        echo '<label for="product_commission">Select a Commission:</label>';
        echo '<select id="product_commission" name="product_commission">';
        echo '<option value="">Choose a Commission</option>';

        foreach ($commissions as $commission_id => $commission_data) {
            $selected = ($product_commission == $commission_id) ? 'selected' : '';
            $commission_title = esc_html($commission_data['title']);
            $min_price = esc_html($commission_data['min_price']);
            $max_price = esc_html($commission_data['max_price']);

            echo '<option value="' . esc_attr($commission_id) . '" ' . $selected . '>' .
                $commission_title . ' (Min: ' . $min_price . ' - Max: ' . $max_price . ')
                </option>';
        }

        echo '</select>';
        echo '</p>';
        echo '</div>';
    }

    /**
     * Save selected commission for the product
     */
    public function save_product_commission($post_id)
    {
        // Get the ID of the current product
        $product_id = isset($_POST['post_ID']) ? $_POST['post_ID'] : $post_id;

        if (isset($_POST['product_commission'])) {
            // Get the selected commission
            $selected_commission_id = sanitize_text_field($_POST['product_commission']);

            // Get the product price
            $product_price = isset($_POST['_regular_price']) ? $_POST['_regular_price'] : 0;

            // Get data for the selected commission
            $commission_min_price = get_post_meta($selected_commission_id, '_product_min_price', true);
            $commission_max_price = get_post_meta($selected_commission_id, '_product_max_price', true);

            // Check if the commission is within the product price range
            if ($product_price < $commission_min_price || $product_price > $commission_max_price) {
                // If the commission doesn't match the acceptable values, update it to empty
                update_post_meta($product_id, '_product_commission', '');
            } else {
                // If the commission is within the acceptable range, update it to the selected commission ID
                update_post_meta($product_id, '_product_commission', $selected_commission_id);
            }
        }
    }

    /**
     * Apply commission to product price
     */
    function apply_commission_to_product_price($price, $product)
    {
        // Get the value of _product_commission for the product
        $product_commission_id = get_post_meta($product->get_id(), '_product_commission', true);

        // Get commission data based on the obtained ID
        $commission_value = get_post_meta($product_commission_id, '_commission_value', true);
        $commission = $price * ($commission_value / 100);

        // Apply the commission to the product price
        $price += $commission;

        return $price;
    }
}