<?php

require_once 'classes/Commissions_Post_Type.php';
require_once 'classes/Product_Commission.php';

class Demo_Controller
{
    /**
     * Controller constructor.
     */
    public function __construct()
    {
        new Commissions_Post_Type();
        new Product_Commission();
    }
}