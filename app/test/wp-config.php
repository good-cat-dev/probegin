<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpress' );

/** Database hostname */
define( 'DB_HOST', 'db' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/** Filesystem Method */
define( 'FS_METHOD', 'direct' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         's#e*8.~=Qs-@cO/oL<Bl6(fb(&}!b&;&uU+qwry.5aEg%b>=dXWkv38cx{g=o+xh' );
define( 'SECURE_AUTH_KEY',  '9oTF/&rM$D#H8,=AtKt=*>{5:=2:S|<3CLEaVVA_$CaN;jfXfFOD}yhMNkkL^wf`' );
define( 'LOGGED_IN_KEY',    '_^)-dHNQ84?n+FBgL(;iD[UXNQkupm7|2bSS|~L:R_{WZF)un?Ku}iH*dzpD@|tY' );
define( 'NONCE_KEY',        '}cp>JLUyLEY>GVG8|x!(aCu~1fR52HyBqbQC@i&)-zL>-4u|JA~=TK{(Xh>;_ ?x' );
define( 'AUTH_SALT',        '^MsI)G^Pa{2j!l+?,n6q[4+!XPUu(X%.3D2>_~Bk1!Q4Y>hDIhO4{5@=;@O{0bMu' );
define( 'SECURE_AUTH_SALT', 'Qd?amOG2;9_m&;&}+POB!n jFIt$+`Oe3*q#Q*sft^L!v.)GMLWVlFZK?KkMqY;c' );
define( 'LOGGED_IN_SALT',   '5j%^*%wWb+LLcrV2RZR6_4&A`Z.uE$I?Vi}H!}rQ%HU;4S1Z3wnkT-/%w^gABX8v' );
define( 'NONCE_SALT',       '/R`E4T:#cmu#dEMKggRoy^bv)^0Z>e*sp4qyUI|MPZlNm[@%!{t[m{)+^}x|N?3!' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'prbgn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
